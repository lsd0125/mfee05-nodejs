const express = require('express');
express.shinderVar = 'shinder';
const url = require('url');
const bodyParser = require('body-parser');
const multer = require('multer');
const fs = require('fs');
const session = require('express-session');
const moment = require('moment-timezone');
const db = require(__dirname + '/_connect_db');
const cors = require('cors');
const axios = require('axios');
const zlib = require('zlib');
const cheerio = require('cheerio');

const upload = multer({dest: 'tmp_uploads/'});

const app = express();

app.set('view engine', 'ejs');
//console.log(process);
// console.log(process.env);
// console.log(process.env.PATH);

app.use(bodyParser.urlencoded({extended: false})); // top level middleware
app.use(bodyParser.json()); // top level middleware
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'skdfjhdksfj',
    cookie: {
        maxAge: 1200000
    }
}));

const whitelist = [
    'http://localhost:63342',
    'http://127.0.0.1:63342',
    'http://localhost:3000',
    'http://localhost:5500',
    'http://127.0.0.1:5500',
    undefined,
];

const corsOptions = {
    credentials: true,
    origin: function(origin, callback){
        console.log('origin:', origin);
        if(whitelist.indexOf(origin) !== -1){
            callback(null, true); // 允許
        } else {
            callback(null, false); // 不允許
        }
    }
};
app.use(cors(corsOptions));

// 取得登入的狀態
app.use((req, res, next)=>{
    res.locals.isLogin = req.session.loginUser || false;
    res.locals.loginData = req.session.loginData || false;
    next();
});

app.get('/', (req, res)=>{
    res.render('home', {name: 'Shinder<br>Lin'});
});

app.get('/try-url', (req, res)=>{
    res.write(req.protocol + '://' + req.get('host') + req.url + '\n');
    res.write(req.protocol + '\n');
    res.write(req.get('host') + '\n');
    res.write(req.url + '\n');
    res.end('');
});

app.get('/data-sales/1', (req, res)=>{
    const sales = require(__dirname + '/../data/sales.json');
    console.log(sales);

    //res.json(sales);
    res.render('data-sales', {sales: sales});
});

app.get('/try-qs', (req, res)=>{
    // const urlParts = url.parse(req.url, true);
    const urlParts = url.parse('https://tw.bid.yahoo.com/coupon/group/33077?clf=1&sort=-buycnt', true);
    console.log(urlParts);
    console.log(urlParts.query.name);
    res.json(urlParts);
});

app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
});
app.post('/try-post-form', (req, res)=>{
    // console.log(req.body);
    // res.json(req.body);
    res.render('try-post-form', req.body);
});

app.get('/sync-async', (req, res)=>{
    setTimeout(()=>{
        res.send('Hello 123');
    }, 5000);

});

app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    const output = {
        success: false,
        url: '',
        msg: '沒有上傳檔案',
    };
    console.log(req.file);
    if(req.file && req.file.originalname){
        switch (req.file.mimetype) {
            case 'image/jpeg':
            case 'image/png':
            case 'image/gif':
                fs.rename(req.file.path, './public/img/'+req.file.originalname, error=>{
                    if(error){
                        output.success = false;
                        output.msg = '無法搬動檔案';
                    } else {
                        output.success = true;
                        output.url = '/img/'+req.file.originalname;
                        output.msg = '';
                    }
                    res.json(output);
                });

                break;
            default:
                fs.unlink(req.file.path, error=>{
                    output.msg = '不接受式這種檔案格';
                    res.json(output);
                });
        }
    } else {
        res.json(output);
    }



});

app.get('/abc', (req, res)=>{
    res.send('Hello~~~~');
});

app.get('/my-params1/:action?/:id?', (req, res)=>{
    res.json(req.params);
});

app.get(/^\/09\d{2}-?\d{3}-?\d{3}$/, (req, res)=>{
    let mobile = req.url.slice(1);
    mobile = mobile.split('?')[0];
    mobile = mobile.split('-').join('');

    res.json(mobile);
});

// const admin1fn = require(__dirname + '/admins/admin1');
// admin1fn(app);
require(__dirname + '/admins/admin1')(app);

app.use( require(__dirname+'/admins/admin2') );
app.use('/admin3/', require(__dirname+'/admins/admin3') );

app.get('/try-session', (req, res)=>{
    console.log(req.session);
    req.session.my_var = req.session.my_var || 0;
    req.session.my_var++;
    res.json({
        my_var: req.session.my_var,
        session: req.session
    });
});

// 管理的登入登出
app.use('/login-test', require(__dirname+'/routers/login-test') );

// 通訊錄
app.use('/address-book', require(__dirname+'/routers/address-book') );



app.get('/try-moment', (req, res)=>{
    const fm = 'YYYY-MM-DD HH:mm:ss';
    const m1 = moment(req.session.cookie.expires);
    const m2 = moment(new Date());
    const m3 = moment('2018-9-2');

    res.json({
        m1: m1.format(fm),
        m2: m2.format(fm),
        m3: m3.format(fm),
        m1_: m1.tz('Europe/London').format(fm),
        m2_: m2.tz('Europe/London').format(fm),
        m3_: m3.tz('Europe/London').format(fm),
    });
});

app.get('/try-db', (req, res)=>{
    const sql = "SELECT * FROM `address_book`";
    db.query(sql, (error, result, fields)=>{
        if(!error){
            res.json(result);
        } else {
            res.end(error);
        }
    });
});

app.get('/try-axios', (req, res)=>{
    axios.get('https://tw.yahoo.com/')
        .then(response=>{
            res.end(response.data);
        })
});

app.get('/try-bus', (req, res)=>{
    axios({
        method: 'get',
        url: 'https://tcgbusfs.blob.core.windows.net/blobbus/GetBusData.gz',
        responseType: 'stream'
    })
        .then(response=>{
            res.writeHead(200, {
               'Content-Type': 'application/json; charset=UTF-8'
            });
            response.data.pipe(zlib.createGunzip()).pipe(res);
        })
});

app.get('/try-cheerio', (req, res)=> {
    const $ = cheerio.load('<h2>Abc<span>def</span></h2>');
    $('span').css('color', 'red');
    res.end($.html());
});

app.get('/try-cheerio2', (req, res)=> {
    axios.get('https://tw.yahoo.com/')
        .then(response=>{
            const $ = cheerio.load(response.data);
            const ar = [];
            const imgs = $('img');

            for(let i=0; i<imgs.length; i++){
                ar.push( imgs.eq(i).attr('src'));
            }
            //res.send( ar.join('<br>') );
            res.send(
                ar.map(el=>`<img src="${el}"><br>`).join('')
            )
        })
});
app.use(express.static(__dirname + '/../public'));

app.use((req, res)=>{
    res.type('text/html');
    res.status(404);
    res.send('<h2>找不到頁面</h2>');

});

app.listen(3000, ()=>{
    console.log('express server start');
});
