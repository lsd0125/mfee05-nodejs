-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 
-- 伺服器版本： 10.4.10-MariaDB
-- PHP 版本： 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `address_book`
--
CREATE DATABASE IF NOT EXISTS `address_book` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `address_book`;

-- --------------------------------------------------------

--
-- 資料表結構 `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL COMMENT '流水號',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用者帳號',
  `pwd` char(40) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用者密碼',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理者帳號';

--
-- 傾印資料表的資料 `admin`
--

INSERT INTO `admin` (`id`, `username`, `pwd`, `created_at`, `updated_at`) VALUES
(1, 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '2019-12-05 00:43:04', '2019-12-05 00:43:04');

-- --------------------------------------------------------

--
-- 資料表結構 `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL COMMENT '流水號',
  `studentId` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學號',
  `studentName` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學生姓名',
  `studentGender` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學生性別',
  `studentBirthday` date NOT NULL COMMENT '學生生日',
  `studentPhoneNumber` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學生手機號碼',
  `studentDescription` text COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '個人描述',
  `studentImg` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '照片檔案名稱',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='學生資料表';

--
-- 傾印資料表的資料 `students`
--

INSERT INTO `students` (`id`, `studentId`, `studentName`, `studentGender`, `studentBirthday`, `studentPhoneNumber`, `studentDescription`, `studentImg`, `created_at`, `updated_at`) VALUES
(2, 'S001', '陳同學', '女', '1995-02-21', '0911111111', '你好，我是陳同學…\r\n請多指教…', NULL, '2019-12-06 00:37:09', '2019-12-10 18:52:53'),
(7, 'S003', '江同學', '女', '2000-07-25', '0966666666', '你好，我是江同學…\r\n請多指教…', NULL, '2019-12-08 22:02:24', '2019-12-10 18:52:58'),
(18, 'S006', '張同學', '女', '1995-07-13', '0987666555', '你好，我是張同學…\r\n請多指教…', NULL, '2019-12-10 18:41:50', '2019-12-10 18:53:04'),
(20, 'S007', 'Alex', '男', '1990-10-07', '0911111111', 'abcd\r\nabcd', '20200110103753.gif', '2020-01-10 10:37:53', '2020-01-10 10:37:53'),
(21, 'S099', 'Bill', '男', '1990-10-07', '0911111111', 'aaaa\r\nbbbb', '20200110103815.jpg', '2020-01-10 10:38:15', '2020-01-10 10:38:15'),
(22, 'S009', 'Carl', '男', '1990-10-07', '0911111111', 'a\r\nb\r\nc', '20200110103839.jpg', '2020-01-10 10:38:39', '2020-01-10 10:38:39');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `studentId` (`studentId`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=2;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=23;
--
-- 資料庫： `company`
--
CREATE DATABASE IF NOT EXISTS `company` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `company`;

-- --------------------------------------------------------

--
-- 資料表結構 `department`
--

CREATE TABLE `department` (
  `Dname` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Dnumber` int(11) NOT NULL,
  `Mgr_ssn` char(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mgr_start_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '結束時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='department';

--
-- 傾印資料表的資料 `department`
--

INSERT INTO `department` (`Dname`, `Dnumber`, `Mgr_ssn`, `Mgr_start_date`, `created_at`, `updated_at`) VALUES
('Headquarters', 1, '888665555', '1981-06-19', '2019-12-02 21:23:56', '2019-12-02 21:23:56'),
('Administration', 4, '987654321', '1995-01-01', '2019-12-02 21:23:56', '2019-12-02 21:23:56'),
('Research', 5, '333445555', '1988-05-22', '2019-12-02 21:23:56', '2019-12-02 21:23:56');

-- --------------------------------------------------------

--
-- 資料表結構 `dependent`
--

CREATE TABLE `dependent` (
  `Essn` char(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Dependent_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Sex` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Bdate` date DEFAULT NULL,
  `Relationship` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '修改時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='dependent';

--
-- 傾印資料表的資料 `dependent`
--

INSERT INTO `dependent` (`Essn`, `Dependent_name`, `Sex`, `Bdate`, `Relationship`, `created_at`, `updated_at`) VALUES
('123456789', 'Alice', 'F', '1988-12-30', 'Daughter', '2019-12-02 21:45:14', '2019-12-02 21:45:14'),
('123456789', 'Elizabeth', 'F', '1967-05-05', 'spouse', '2019-12-02 21:45:14', '2019-12-02 21:45:14'),
('123456789', 'Michael', 'M', '1988-01-04', 'Son', '2019-12-02 21:45:14', '2019-12-02 21:45:14'),
('333445555', 'Alice', 'F', '1986-04-05', 'Daughter', '2019-12-02 21:41:37', '2019-12-02 21:41:37'),
('333445555', 'Joy', 'F', '1958-05-03', 'Spouse', '2019-12-02 21:45:14', '2019-12-02 21:45:14'),
('333445555', 'Theodore', 'M', '1983-10-25', 'Son', '2019-12-02 21:41:37', '2019-12-02 21:41:37'),
('987654321', 'Abner', 'M', '1942-02-28', 'Spouse', '2019-12-02 21:45:14', '2019-12-02 21:45:14');

-- --------------------------------------------------------

--
-- 資料表結構 `dept_locations`
--

CREATE TABLE `dept_locations` (
  `Dnumber` int(11) NOT NULL,
  `Dlocation` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='dept_locations';

--
-- 傾印資料表的資料 `dept_locations`
--

INSERT INTO `dept_locations` (`Dnumber`, `Dlocation`, `created_at`, `updated_at`) VALUES
(1, 'Houston', '2019-12-02 21:25:12', '2019-12-02 21:25:12'),
(4, 'Stafford', '2019-12-02 21:25:12', '2019-12-02 21:25:12'),
(5, 'Bellaire', '2019-12-02 21:25:12', '2019-12-02 21:25:12'),
(5, 'Houston', '2019-12-02 21:25:12', '2019-12-02 21:25:12'),
(5, 'Sugarland', '2019-12-02 21:25:12', '2019-12-02 21:25:12');

-- --------------------------------------------------------

--
-- 資料表結構 `employee`
--

CREATE TABLE `employee` (
  `Fname` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Minit` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Lname` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Ssn` char(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Bdate` date DEFAULT NULL,
  `Address` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Sex` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Salary` decimal(10,2) DEFAULT NULL,
  `Super_ssn` char(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dno` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='EMPLOYEE';

--
-- 傾印資料表的資料 `employee`
--

INSERT INTO `employee` (`Fname`, `Minit`, `Lname`, `Ssn`, `Bdate`, `Address`, `Sex`, `Salary`, `Super_ssn`, `Dno`, `created_at`, `updated_at`) VALUES
('John', 'B', 'Smith', '123456789', '1965-01-09', '731 Fondren, Houston, TX', 'M', '30000.00', '333445555', 5, '2019-12-02 21:11:36', '2019-12-02 21:11:36'),
('Franklin', 'T', 'Wong', '333445555', '1955-12-08', '638 Voss, houston, TX', 'M', '40000.00', '888665555', 5, '2019-12-02 21:14:28', '2019-12-02 21:14:28'),
('Joyce', 'A', 'English', '453453453', '1972-07-31', '5631 Rice, Houston, TX', 'F', '25000.00', '333445555', 5, '2019-12-02 21:22:14', '2019-12-02 21:22:14'),
('Ramesh', 'K', 'Narayan', '666884444', '1962-09-15', '975 Fire Oak, Humble, TX', 'M', '38000.00', '333445555', 5, '2019-12-02 21:22:14', '2019-12-02 21:22:14'),
('James', 'E', 'Borg', '888665555', '1937-11-10', '450 Stone, Houston, TX', 'M', '55000.00', NULL, 1, '2019-12-02 21:22:14', '2019-12-02 21:22:14'),
('Jennifer', 'S', 'Wallace', '987654321', '1941-06-20', '291 Berry, Bellaire, TX', 'F', '43000.00', '888665555', 4, '2019-12-02 21:22:14', '2019-12-02 21:22:14'),
('Ahmad', 'V', 'Jabbar', '987987987', '1969-03-29', '980 Dallas, Houston, TX', 'M', '25000.00', '987654321', 4, '2019-12-02 21:22:14', '2019-12-31 10:31:51'),
('Alicia', 'J', 'Zelaya', '999887777', '1968-01-19', '3321 Castle, Spring, TX', 'F', '25000.00', '987654321', 4, '2019-12-02 21:14:28', '2019-12-02 21:14:28');

-- --------------------------------------------------------

--
-- 資料表結構 `project`
--

CREATE TABLE `project` (
  `Pname` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Pnumber` int(11) NOT NULL,
  `Plocation` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dnum` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='project';

--
-- 傾印資料表的資料 `project`
--

INSERT INTO `project` (`Pname`, `Pnumber`, `Plocation`, `Dnum`, `created_at`, `updated_at`) VALUES
('ProductX', 1, 'Bellaire', 5, '2019-12-02 21:36:29', '2019-12-02 21:36:29'),
('ProductY', 2, 'Sugarland', 5, '2019-12-02 21:36:29', '2019-12-02 21:36:29'),
('ProductZ', 3, 'Houston', 5, '2019-12-02 21:36:29', '2019-12-02 21:36:29'),
('Computerization', 10, 'Stafford', 4, '2019-12-02 21:36:29', '2019-12-02 21:36:29'),
('Reorganization', 20, 'Houston', 1, '2019-12-02 21:36:29', '2019-12-02 21:36:29'),
('Newbenefits', 30, 'Stafford', 4, '2019-12-02 21:36:29', '2019-12-02 21:36:29');

-- --------------------------------------------------------

--
-- 資料表結構 `works_on`
--

CREATE TABLE `works_on` (
  `Essn` char(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Pno` int(11) NOT NULL,
  `Hours` decimal(3,1) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '修改時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='works_on';

--
-- 傾印資料表的資料 `works_on`
--

INSERT INTO `works_on` (`Essn`, `Pno`, `Hours`, `created_at`, `updated_at`) VALUES
('123456789', 1, '32.5', '2019-12-02 21:26:47', '2019-12-02 21:26:47'),
('123456789', 2, '7.5', '2019-12-02 21:26:47', '2019-12-02 21:26:47'),
('333445555', 2, '10.0', '2019-12-02 21:29:38', '2019-12-02 21:29:38'),
('333445555', 3, '10.0', '2019-12-02 21:29:38', '2019-12-02 21:29:38'),
('333445555', 10, '10.0', '2019-12-02 21:29:38', '2019-12-02 21:29:38'),
('333445555', 20, '10.0', '2019-12-02 21:29:38', '2019-12-02 21:29:38'),
('453453453', 1, '20.0', '2019-12-02 21:26:47', '2019-12-02 21:26:47'),
('453453453', 2, '20.0', '2019-12-02 21:26:47', '2019-12-02 21:26:47'),
('666884444', 3, '40.0', '2019-12-02 21:26:47', '2019-12-02 21:26:47'),
('888665555', 20, '0.0', '2019-12-02 21:33:12', '2019-12-02 21:33:12'),
('987654321', 20, '15.0', '2019-12-02 21:33:12', '2019-12-02 21:33:12'),
('987654321', 30, '20.0', '2019-12-02 21:33:12', '2019-12-02 21:33:12'),
('987987987', 10, '35.0', '2019-12-02 21:33:12', '2019-12-02 21:33:12'),
('987987987', 30, '5.0', '2019-12-02 21:33:12', '2019-12-02 21:33:12'),
('99887777', 30, '30.0', '2019-12-02 21:29:38', '2019-12-02 21:29:38'),
('999887777', 10, '10.0', '2019-12-02 21:33:12', '2019-12-02 21:33:12');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`Dnumber`);

--
-- 資料表索引 `dependent`
--
ALTER TABLE `dependent`
  ADD PRIMARY KEY (`Essn`,`Dependent_name`);

--
-- 資料表索引 `dept_locations`
--
ALTER TABLE `dept_locations`
  ADD PRIMARY KEY (`Dnumber`,`Dlocation`);

--
-- 資料表索引 `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`Ssn`);

--
-- 資料表索引 `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`Pnumber`),
  ADD UNIQUE KEY `Pname` (`Pname`);

--
-- 資料表索引 `works_on`
--
ALTER TABLE `works_on`
  ADD PRIMARY KEY (`Essn`,`Pno`);
--
-- 資料庫： `my_db`
--
CREATE DATABASE IF NOT EXISTS `my_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `my_db`;

-- --------------------------------------------------------

--
-- 資料表結構 `courses`
--

CREATE TABLE `courses` (
  `cId` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '課程編號',
  `cName` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '課程名稱',
  `credit` tinyint(1) NOT NULL COMMENT '學分數',
  `isCompulsory` tinyint(1) NOT NULL COMMENT '必選修',
  `tId` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '老師編號',
  `created_at` datetime NOT NULL COMMENT '新增時間',
  `updated_at` datetime NOT NULL COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='課程資料表';

--
-- 傾印資料表的資料 `courses`
--

INSERT INTO `courses` (`cId`, `cName`, `credit`, `isCompulsory`, `tId`, `created_at`, `updated_at`) VALUES
('C001', '程式設計', 4, 1, 'T001', '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
('C002', '網頁設計', 3, 1, 'T002', '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
('C003', '視覺設計', 2, 1, 'T003', '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
('C004', '網路教學', 4, 1, 'T005', '2019-12-02 00:00:00', '2019-12-02 00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `scores`
--

CREATE TABLE `scores` (
  `sId` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學生編號',
  `cId` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '課程編號',
  `score` tinyint(3) NOT NULL COMMENT '成績',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='成績資料表';

--
-- 傾印資料表的資料 `scores`
--

INSERT INTO `scores` (`sId`, `cId`, `score`, `created_at`, `updated_at`) VALUES
('S002', 'C002', 63, '2019-12-02 11:12:35', '2019-12-30 11:14:45'),
('S005', 'C001', 74, '2019-12-02 11:11:59', '2019-12-30 11:15:13'),
('S005', 'C002', 93, '2019-12-02 11:12:35', '2019-12-30 11:15:16'),
('S007', 'C004', 94, '2019-12-02 11:13:06', '2019-12-30 11:15:41'),
('S088', 'C003', 82, '2019-12-02 11:13:06', '2019-12-30 11:15:21');

-- --------------------------------------------------------

--
-- 資料表結構 `students`
--

CREATE TABLE `students` (
  `sId` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學生編號',
  `sName` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學生姓名',
  `sGender` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學生性別',
  `sAge` tinyint(3) DEFAULT NULL,
  `sNickname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '學生暱稱',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='學生資料表';

--
-- 傾印資料表的資料 `students`
--

INSERT INTO `students` (`sId`, `sName`, `sGender`, `sAge`, `sNickname`, `created_at`, `updated_at`) VALUES
('S003', '王○○', '男', 24, '小王', '2019-12-02 18:27:16', '2019-12-27 15:50:54'),
('S004', '江○○', '女', 25, '小江', '2019-12-02 18:27:16', '2019-12-27 15:51:05'),
('S005', '周○○', '女', 22, '小周', '2019-12-02 18:27:16', '2019-12-27 15:52:09'),
('S006', '黃○○', '男', 21, '小黃', '2019-12-02 18:27:16', '2019-12-27 16:16:00'),
('S007', '丁○○', '男', 23, '老王', '2019-12-02 18:27:16', '2019-12-30 10:19:53'),
('S008', '鄭○○', '男', 23, '小鄭', '2019-12-02 18:27:16', '2019-12-27 16:16:06'),
('S087', '楊○○', '男', 21, '好人', '2019-12-02 11:11:37', '2019-12-27 16:16:09'),
('S088', '陳○○', '女', 20, '小白', '2019-12-02 11:11:37', '2019-12-27 16:16:11');

-- --------------------------------------------------------

--
-- 資料表結構 `teachers`
--

CREATE TABLE `teachers` (
  `tId` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '老師編號',
  `tName` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '老師姓名',
  `created_at` datetime NOT NULL COMMENT '新增時間',
  `updated_at` datetime NOT NULL COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='老師資料表';

--
-- 傾印資料表的資料 `teachers`
--

INSERT INTO `teachers` (`tId`, `tName`, `created_at`, `updated_at`) VALUES
('T001', '黃○○', '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
('T002', '林○○', '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
('T003', '王○○', '2019-12-02 00:00:00', '2019-12-02 00:00:00'),
('T005', '謝○○', '2019-12-02 00:00:00', '2019-12-02 00:00:00');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`cId`,`tId`);

--
-- 資料表索引 `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`sId`,`cId`);

--
-- 資料表索引 `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`sId`);

--
-- 資料表索引 `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`tId`);
--
-- 資料庫： `shopping_cart`
--
CREATE DATABASE IF NOT EXISTS `shopping_cart` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `shopping_cart`;

-- --------------------------------------------------------

--
-- 資料表結構 `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL COMMENT '流水號',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用者帳號',
  `pwd` char(40) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用者密碼',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '管理者姓名',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理者帳號';

--
-- 傾印資料表的資料 `admin`
--

INSERT INTO `admin` (`id`, `username`, `pwd`, `name`, `created_at`, `updated_at`) VALUES
(1, 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '我是管理者', '2019-11-25 13:18:46', '2019-12-16 01:34:25');

-- --------------------------------------------------------

--
-- 資料表結構 `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL COMMENT '流水號',
  `categoryName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '類別名稱',
  `categoryParentId` int(11) DEFAULT 0 COMMENT '上層編號',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='類別資料表';

--
-- 傾印資料表的資料 `categories`
--

INSERT INTO `categories` (`categoryId`, `categoryName`, `categoryParentId`, `created_at`, `updated_at`) VALUES
(1, '全部', 0, '2019-12-30 13:49:34', '2019-12-30 13:50:19'),
(2, 'PS4aaa', 1, '2019-12-30 13:49:53', '2020-01-13 10:35:19'),
(3, 'Switch', 1, '2019-12-30 13:53:30', '2019-12-30 13:53:30'),
(4, 'white_color', 2, '2020-01-13 09:18:55', '2020-01-13 09:18:55'),
(7, 'dark_color', 2, '2020-01-13 09:55:55', '2020-01-13 09:55:55');

-- --------------------------------------------------------

--
-- 資料表結構 `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL COMMENT '流水號',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '內容',
  `rating` tinyint(1) NOT NULL COMMENT '評分',
  `parentId` int(11) NOT NULL DEFAULT 0 COMMENT '上(父)層編號',
  `itemId` int(11) NOT NULL COMMENT '商品編號',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='評論';

--
-- 傾印資料表的資料 `comments`
--

INSERT INTO `comments` (`id`, `name`, `content`, `rating`, `parentId`, `itemId`, `created_at`, `updated_at`) VALUES
(1, 'Bill', 'Hello World!!', 2, 0, 1, '2019-12-30 13:57:03', '2019-12-30 13:57:03'),
(2, '管理員', 'Hi, there!\r\n\r\nFrom Darren Yang', 0, 1, 1, '2019-12-30 13:57:56', '2019-12-30 13:57:56'),
(3, 'Darren', 'Hello World \r\nXDDDD', 3, 0, 1, '2020-01-13 15:49:13', '2020-01-13 16:03:07'),
(4, '管理員', 'Hello World +1', 0, 3, 1, '2020-01-13 16:02:43', '2020-01-13 16:02:43'),
(5, '管理員', '+1+1', 0, 3, 1, '2020-01-13 16:04:20', '2020-01-13 16:04:20');

-- --------------------------------------------------------

--
-- 資料表結構 `items`
--

CREATE TABLE `items` (
  `itemId` int(11) NOT NULL COMMENT '流水號',
  `itemName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品名稱',
  `itemImg` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品照片路徑',
  `itemPrice` int(11) NOT NULL COMMENT '商品價格',
  `itemQty` tinyint(3) NOT NULL COMMENT '商品數量',
  `itemCategoryId` int(11) NOT NULL COMMENT '商品種類編號',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='商品列表';

--
-- 傾印資料表的資料 `items`
--

INSERT INTO `items` (`itemId`, `itemName`, `itemImg`, `itemPrice`, `itemQty`, `itemCategoryId`, `created_at`, `updated_at`) VALUES
(1, 'PS4_白', 'item_20191230065212.jpg', 21000, 5, 2, '2019-12-30 13:52:12', '2019-12-30 13:52:12'),
(2, 'Switch_灰', 'item_20191230065359.jpg', 10000, 4, 3, '2019-12-30 13:53:59', '2019-12-30 13:53:59');

-- --------------------------------------------------------

--
-- 資料表結構 `item_lists`
--

CREATE TABLE `item_lists` (
  `itemListId` int(11) NOT NULL COMMENT '流水號',
  `orderId` int(11) NOT NULL COMMENT '訂單編號',
  `itemId` int(11) NOT NULL COMMENT '商品編號',
  `checkPrice` int(11) NOT NULL COMMENT '結帳時單價',
  `checkQty` tinyint(3) NOT NULL COMMENT '結帳時數量',
  `checkSubtotal` int(11) NOT NULL COMMENT '結帳時小計',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='訂單中的商品列表';

--
-- 傾印資料表的資料 `item_lists`
--

INSERT INTO `item_lists` (`itemListId`, `orderId`, `itemId`, `checkPrice`, `checkQty`, `checkSubtotal`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 21000, 3, 63000, '2019-12-30 13:56:37', '2019-12-30 13:56:37'),
(2, 2, 1, 21000, 3, 63000, '2020-01-13 14:33:20', '2020-01-13 14:33:20'),
(3, 2, 2, 10000, 1, 10000, '2020-01-13 14:33:20', '2020-01-13 14:33:20'),
(4, 3, 2, 10000, 1, 10000, '2020-01-13 14:45:03', '2020-01-13 14:45:03');

-- --------------------------------------------------------

--
-- 資料表結構 `multiple_images`
--

CREATE TABLE `multiple_images` (
  `multipleImageId` int(11) NOT NULL COMMENT '流水號',
  `multipleImageImg` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '圖片名稱',
  `itemId` int(11) NOT NULL COMMENT '商品編號',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='商品圖片';

--
-- 傾印資料表的資料 `multiple_images`
--

INSERT INTO `multiple_images` (`multipleImageId`, `multipleImageImg`, `itemId`, `created_at`, `updated_at`) VALUES
(1, 'multiple_images_20191230065238_0.jpg', 1, '2019-12-30 13:52:38', '2019-12-30 13:52:38'),
(2, 'multiple_images_20191230065238_1.jpg', 1, '2019-12-30 13:52:38', '2019-12-30 13:52:38'),
(3, 'multiple_images_20191230065238_2.jpg', 1, '2019-12-30 13:52:38', '2019-12-30 13:52:38'),
(4, 'multiple_images_20191230065238_3.jpg', 1, '2019-12-30 13:52:38', '2019-12-30 13:52:38'),
(5, 'multiple_images_20191230065238_4.jpg', 1, '2019-12-30 13:52:38', '2019-12-30 13:52:38'),
(6, 'multiple_images_20191230065238_5.jpg', 1, '2019-12-30 13:52:38', '2019-12-30 13:52:38'),
(7, 'multiple_images_20191230065239_6.jpg', 1, '2019-12-30 13:52:39', '2019-12-30 13:52:39'),
(8, 'multiple_images_20191230065239_7.jpg', 1, '2019-12-30 13:52:39', '2019-12-30 13:52:39'),
(9, 'multiple_images_20191230065239_8.jpg', 1, '2019-12-30 13:52:39', '2019-12-30 13:52:39');

-- --------------------------------------------------------

--
-- 資料表結構 `orders`
--

CREATE TABLE `orders` (
  `orderId` int(11) NOT NULL COMMENT '流水號',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用者帳號',
  `paymentTypeId` int(11) NOT NULL COMMENT '付款方式',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='結帳資料表';

--
-- 傾印資料表的資料 `orders`
--

INSERT INTO `orders` (`orderId`, `username`, `paymentTypeId`, `created_at`, `updated_at`) VALUES
(1, 'bill', 1, '2019-12-30 13:56:37', '2019-12-30 13:56:37'),
(2, 'darren', 2, '2020-01-13 14:33:20', '2020-01-13 14:33:20'),
(3, 'darren', 1, '2020-01-13 14:45:03', '2020-01-13 14:45:03');

-- --------------------------------------------------------

--
-- 資料表結構 `payment_types`
--

CREATE TABLE `payment_types` (
  `paymentTypeId` int(11) NOT NULL COMMENT '流水號',
  `paymentTypeName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '付款方式名稱',
  `paymentTypeImg` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '付款方式圖片名稱',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='付款方式';

--
-- 傾印資料表的資料 `payment_types`
--

INSERT INTO `payment_types` (`paymentTypeId`, `paymentTypeName`, `paymentTypeImg`, `created_at`, `updated_at`) VALUES
(1, 'LINE Pay', 'payment_type_20191230065040.png', '2019-12-30 13:50:40', '2019-12-30 13:50:40'),
(2, 'Samsung Pay', 'payment_type_20191230065105.png', '2019-12-30 13:51:05', '2019-12-30 13:51:05'),
(3, '現金', 'payment_type_20191230065128.png', '2019-12-30 13:51:28', '2019-12-30 13:51:28');

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT '流水號',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用者名稱',
  `pwd` char(40) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用者密碼',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
  `gender` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '性別',
  `phoneNumber` int(11) NOT NULL COMMENT '手機號碼',
  `birthday` datetime NOT NULL COMMENT '出生年月日',
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '地址',
  `isActivated` tinyint(1) NOT NULL DEFAULT 0 COMMENT '開通狀況',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT '新增時間',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='使用者資料表';

--
-- 傾印資料表的資料 `users`
--

INSERT INTO `users` (`id`, `username`, `pwd`, `name`, `gender`, `phoneNumber`, `birthday`, `address`, `isActivated`, `created_at`, `updated_at`) VALUES
(1, 'bill', 'c692d6a10598e0a801576fdd4ecf3c37e45bfbc4', 'bill', '男', 911222333, '2000-10-10 00:00:00', '地球村', 0, '2019-12-30 13:56:28', '2019-12-30 13:56:28'),
(2, 'darren', '39afb6cb2f84a5645c2345b80cbfe0b45648600b', 'Darren', '男', 911222333, '2000-10-10 00:00:00', '地球村', 0, '2020-01-13 14:32:56', '2020-01-13 14:32:56');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- 資料表索引 `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- 資料表索引 `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`itemId`);

--
-- 資料表索引 `item_lists`
--
ALTER TABLE `item_lists`
  ADD PRIMARY KEY (`itemListId`);

--
-- 資料表索引 `multiple_images`
--
ALTER TABLE `multiple_images`
  ADD PRIMARY KEY (`multipleImageId`);

--
-- 資料表索引 `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderId`);

--
-- 資料表索引 `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`paymentTypeId`);

--
-- 資料表索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=2;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=9;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=6;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `items`
--
ALTER TABLE `items`
  MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=3;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `item_lists`
--
ALTER TABLE `item_lists`
  MODIFY `itemListId` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=5;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `multiple_images`
--
ALTER TABLE `multiple_images`
  MODIFY `multipleImageId` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=10;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `orders`
--
ALTER TABLE `orders`
  MODIFY `orderId` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=4;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `paymentTypeId` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=4;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=3;
--
-- 資料庫： `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

-- --------------------------------------------------------

--
-- 資料表結構 `address_book`
--

CREATE TABLE `address_book` (
  `sid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `address_book`
--

INSERT INTO `address_book` (`sid`, `name`, `email`, `mobile`, `birthday`, `address`, `created_at`) VALUES
(1, '李小明', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(2, '李小明2', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(3, '李小明3', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(4, '李小明4', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(5, '李小明5', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(6, '李小明6', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(7, '李小明7', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(8, '李小明8', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(9, '李小明9', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(10, '李小明10', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(11, '李小明11', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(12, '李小明12', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(13, '李小明13', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(14, '陳小華2', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(15, '陳小華3', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(16, '陳小華4', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(17, '陳小華5', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(18, '陳小華6', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(19, '陳小華7', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(20, '陳小華8', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(21, '陳小華9', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(22, '陳小華10', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(23, '陳小華11', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(24, '陳小華12', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00'),
(25, '陳小華13', 'dsfgdsf@sdfs.com', '0918111222', '2000-01-02', '', '2020-02-17 07:10:00');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`sid`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `address_book`
--
ALTER TABLE `address_book`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
